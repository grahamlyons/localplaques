<?php

class LocalPlaques_Bootstrap {

    protected $_front;

    public function __construct() {
        require_once 'LocalPlaques/Autoloader.php';
        $autoload = new LocalPlaques_Autoloader();
    }

    protected function _front() {
        if(!$this->_front) {
            $this->_front = Zend_Controller_Front::getInstance();
        }
        return $this->_front;
    }

	protected function _initLayout(){
		Zend_Layout::startMvc();
	}

    protected function _initRoutes(){
		$config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/routes.ini');
		$router = $this->_front()->getRouter();
		$router->removeDefaultRoutes();
		$router->addConfig($config);
		$defaults = $router->getRoutes();
	}

    protected function _configure() {
        $this->_front()
            ->setControllerDirectory(APPLICATION_PATH . '/controllers/')
            ->returnResponse(true);
        $this->_initRoutes();
        $this->_initLayout();
    }

    protected function _processResponse($response) {
        $response->setHeader('Cache-control', 'max-age=30');
        $hash = '"'.md5($response->getBody()).'"';
        $response->setHeader('Etag', $hash);
        $request = $this->_front()->getRequest();
        $etag = $request->getHeader('If-None-Match');
        if($etag && $etag == $hash) {
            $response->setHttpResponseCode(304, 'Not Modified');
            $response->setBody('');
        }
        return $response;
    }

    public function run() {
        $this->_configure();
		$front = $this->_front();
        $response = $front->dispatch();
        $this->_processResponse($response)->sendResponse();
    }

}
