<?php

class LocalPlaques_Autoloader {

    /**
     * Register the autoloader when the class is constructed.
     */
    public function __construct () {
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    /**
     * The autoload function:
     *  converts Class_Name to Class/Name.php
     *  then requires it.
     */
    public static function autoload ($class_name) {
        $filename = preg_replace('@_@', '/', $class_name);
        $filename .= '.php';
        require_once($filename);
    }

}
