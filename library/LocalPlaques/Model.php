<?php

class LocalPlaques_Model {
	
	const JSON_TYPE = 'application/json';
	
	protected $TYPE_ARRAY = Zend_Json::TYPE_ARRAY;
	protected $TYPE_OBJECT = Zend_Json::TYPE_OBJECT;
	protected $TYPE_RAW = 'raw';
	
	protected $_options;
	
	/**
	 * @var Zend_Http_Client
	 */
	protected $_httpClient;
	
	/**
	 * Could theoretically swap this out. Realistically methods are tied to its API.
	 * @var string
	 */
	protected $_httpClientClass = 'Zend_Http_Client';
	
	public function __construct() {
		$this->_options = array('returntype'=>'array');
	}
	
	public function getHttpClient(){
		if(!$this->_httpClient){
			$this->_httpClient = new $this->_httpClientClass();
		}
		return $this->_httpClient;
	}
	
	/**
	 * Get data from a URL and return an array.
	 * @param string $url
	 * @param array $options - just returntype so far.
	 * @return array
	 */
	public function get($url, $options=array()){
		$localOptions = array_merge($this->_options, $options);
		$response = $this->_getUrl($url);
		if(!$response->isSuccessful() || strpos($response->getHeader('Content-type'), self::JSON_TYPE) === false){
			return array();
		}
		return $this->_decodeJson($response->getBody(),$localOptions['returntype']);
	}
	
	protected function _getUrl($url){
		$httpClient = $this->getHttpClient();
		$httpClient->setUri($url);
		return $httpClient->request();
	}
	
	protected function _decodeJson($json, $returntype){
		$type = $this->_getReturnType($returntype);
        if ($type == 'raw') {
            return $json;
        }
		return Zend_Json::decode($json, $type);
	}
	
	protected function _getReturnType($returntype){
		switch ($returntype) {
			case 'array':
				return $this->TYPE_ARRAY;
			break;
			
			case 'object':
				return $this->TYPE_OBJECT;
			break;
			
			case 'raw':
				return $this->TYPE_RAW;
			break;
			
			default:
				throw new Exception('Unrecognised type - ' . $returntype . ' Array or object only.');
			break;
		}
	}
}
