<?php

class LocalPlaques_Log {

    public function crit($message, $detail) {
        error_log('[CRITICAL]' . $message . ' - ' . $detail);
    }

}
