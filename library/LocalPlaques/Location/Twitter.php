<?php

class LocalPlaques_Location_Twitter {

	protected $_searchUrl = 'http://search.twitter.com/search.json?q=';
	
	/**
	 * Returns last known coordinates for a Twitter username.
	 * @param string $user
	 * @return array(float,float)
	 */
	public function getGEOLoc($user){
		
		$tweets = $this->getTweets($user);
	
		$result = array();
		
		$time;
		
		forEach($tweets as $tweet)
		{
		
		if($tweet->geo != NULL && $tweet->from_user == $user){
			
			$created = $tweet->created_at;
			
			if($time == null || $created > $time)
			$time = $created;
			$result = $tweet->geo->coordinates;					
			}
		}
		
		return $result;
	}
	
	/**
	 * 
	 * @param string $user
	 * @return object 
	 */
	public function getTweets($user){
		
		$search = $this->_searchUrl.$user;
		
		$model = new LocalPlaques_Model();
		$content = $model->get($search, array('returntype'=>'object'));
		
		return $content->results;
	}
		
}