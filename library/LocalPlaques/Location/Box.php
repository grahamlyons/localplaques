<?php

class LocalPlaques_Location_Box {
	
	protected $_boxSize = 0.0500;
	
    /**
     * <code>
     * $lat = 51.5382;
     * $lng = -0.1417;
     * $plaquesData = new LocalPlaques_Location_Box();
     * $topLeft = $plaquesData->getBottomRight($lat, $lng);
     * echo $topLeft['lat'] . ', ' . $topLeft['lng'];
     * //expects:
     * //51.5282, -0.1317
     * </code>
     * 
     * @param $lat
     * @param $lng
     * @return array('lat'=>x, 'lng'=>y)
     */
	public function getBottomRight($lat,$lng){
		$modifier = ($this->_boxSize/2);
		return array('lat'=>$lat-$modifier,'lng'=>$lng+$modifier);
	}

    /**
     * <code>
     * $lat = 51.5382;
     * $lng = -0.1417;
     * $plaquesData = new LocalPlaques_Location_Box();
     * $bottomRight = $plaquesData->getTopLeft($lat, $lng);
     * echo $bottomRight['lat'] . ', ' . $bottomRight['lng'];
     * //expects:
     * //51.5482, -0.1517
     * </code>
     * 
     * @param $lat
     * @param $lng
     * @return array('lat'=>x, 'lng'=>y)
     */
	public function getTopLeft($lat,$lng){
		$modifier = ($this->_boxSize/2);
		return array('lat'=>$lat+$modifier,'lng'=>$lng-$modifier);
	}
	
}
