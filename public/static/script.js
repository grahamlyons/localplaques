/**
 * getPosition
 * getPlaques
 * setMap
 */

var Position = function(navigator) {
    this.navigator = navigator;
}

Position.prototype.hasGeo = function() {
    var hasGeo;
    (this.navigator.geolocation) ? 
        hasGeo = true :
        hasGeo = false;
    return hasGeo;
}

Position.prototype.getCoords = function(callback, errback, watch) {
    var self = this,
        func = 'getCurrentPosition';
    
    if(watch) {
        func = 'watchPosition';
    }
    this.navigator.geolocation[func](
        function(position) {
            self.coords = position.coords;
            callback(position.coords)
        }, errback);
}

Position.prototype.getMap = function() {
    if(this.map) {
        return this.map;
    }
    if(this.coords) {
        var latlng = new google.maps.LatLng(this.coords.latitude, this.coords.longitude),
            options = {
                zoom: 14,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }; 

        this.map = new google.maps.Map(document.getElementById('map_canvas'), options);

        marker = new google.maps.Marker({
            position: latlng,
            map: this.map,
            title: 'You are here'
        });

        return this.map;
    }
}

Position.prototype.addPlaques = function() {

    var url = plaques.getLocalUrl(this.coords.latitude, this.coords.longitude);

    plaques.getData(url);

}


/**
* @param object pos
* @param string title
*/
Position.prototype.addMarker = function(pos, title){
    var latlng = new google.maps.LatLng(pos.lat,pos.lng);
    var marker = new google.maps.Marker({
        position: latlng,
        map: this.getMap(),
        title: title
    });

    google.maps.event.addListener(marker, 'click', function() {
        var info = new google.maps.InfoWindow({content: title});
        info.open(this.getMap(), marker);
    });
}


var Plaques = function(document) {
    this.document = document;
    this.localUrl = this.document.location.href + 'plaques/';
}

Plaques.prototype.getLocalUrl = function(lat, lng) {
    var parts = [];
    
    parts.push(this.localUrl);
    parts.push(lat);
    parts.push('/');
    parts.push(lng);
    parts.push('?callback=_callback');

    return parts.join('');
}

Plaques.prototype.getData = function(url) {
    var head = document.getElementsByTagName('head')[0],
        script = document.createElement('script');
    
    script.type = 'text/javascript';
    script.src = url;

    head.appendChild(script);
}

var position = new Position(navigator),
    plaques;

var _callback = function(data) {
    var i, plaque, pos, title;

    for (i in data) {
        plaque = data[i];
        pos = {};
        pos.lat = plaque['plaque']['latitude'];
        pos.lng = plaque['plaque']['longitude'];
        title = plaque['plaque']['inscription'];
        position.addMarker(pos, title);
    }

}

var init = function() {
    /**
     * Initialise
     */
    if (position.hasGeo()) {
        plaques = new Plaques(document);
        position.getCoords(function(coords) {
            var map = position.getMap();
            position.addPlaques();
        }, function(){console.log('Error getting location')}, true);
    } else {
        console.log('Geolocation not available');
        // var g = new google.maps.GeoCoder(),
        //     postcode = 'SW18 4NP';
        // g.geocode({address: postcode}, function(data, status) {console.log(status); console.log(data)})
    }

}

if (define && define.amd) {
    define(init());
}
