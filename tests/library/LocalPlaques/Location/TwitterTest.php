<?php
require_once '/Users/gram/Sites/localplaques/tests/library/bootstrap.php';

class LocalPlaques_Location_TwitterTest extends PHPUnit_Framework_TestCase {

	public function testgetGEOLocReturnsArray(){
		
		$twitter = new LocalPlaques_Location_Twitter();
		$result = $twitter->getGEOLoc('grahamlyons');
		$this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_ARRAY, $result);
		
	}
	
}