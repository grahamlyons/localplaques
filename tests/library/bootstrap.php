<?php 
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));
    
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

require_once('Zend/Loader/Autoloader.php');

$autoload = Zend_Loader_Autoloader::getInstance();
$autoload->registerNamespace('LocalPlaques_');
$autoload->registerNamespace('PHPUnit_');

?>