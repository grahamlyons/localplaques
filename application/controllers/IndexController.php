<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        
    }

    public function indexAction()
    {

    }
    
    public function plaquesAction(){
    	$request = $this->getRequest();
    	
		$lat = $request->getParam('lat');
		$lng = $request->getParam('lng');
		$callback = $request->getParam('callback');

    	$box = new LocalPlaques_Location_Box();
    	$topLeft = $box->getTopLeft($lat,$lng);
    	$bottomRight = $box->getBottomRight($lat,$lng);
        
    	$model = new LocalPlaques_Model();
    	$plaquesUrl = 'http://openplaques.org/plaques.json';
        $url = $plaquesUrl.'?box=['.$topLeft['lat'].','.$topLeft['lng'].'],['.$bottomRight['lat'].','.$bottomRight['lng'].']';
        $data = $model->get($url, array('returntype' => 'raw'));

        if ($callback) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $response = $this->getResponse();
            $response
                ->setHeader('Content-type', 'application/json')
                ->setBody($callback . '(' . $data . ');');
        } else {
            $this->view->assign('plaques',$data);
            $this->view->assign('centre',array('lat'=>$lat, 'lng'=>$lng));
        }
    }

    public function twitterAction()
    {
    	$request = $this->getRequest();
        
    	if($request->isPost()){
    		
    		$username = $request->getParam('username');
    		$twitter = new LocalPlaques_Location_Twitter();
        	$results = $twitter->getGEOLoc($username);
        	
        	$this->view->assign('lat',$results[0]);
        	$this->view->assign('lng',$results[1]);
        }
    	
    }

}

